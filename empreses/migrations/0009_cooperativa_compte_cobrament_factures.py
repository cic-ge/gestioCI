# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('empreses', '0008_remove_empresa_compte_corrent'),
    ]

    operations = [
        migrations.AddField(
            model_name='cooperativa',
            name='compte_cobrament_factures',
            field=models.CharField(help_text='Codi IBAN', max_length=34, null=True, blank=True),
        ),
    ]
