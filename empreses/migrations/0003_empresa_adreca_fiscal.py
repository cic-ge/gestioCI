# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0006_adrecafiscal'),
        ('empreses', '0002_auto_20150629_1815'),
    ]

    operations = [
        migrations.AddField(
            model_name='empresa',
            name='adreca_fiscal',
            field=models.ForeignKey(verbose_name=b'Adre\xc3\xa7a fiscal', blank=True, to='socies.AdrecaFiscal', null=True),
        ),
    ]
