# coding=utf-8

from django import forms
from django.core.exceptions import ValidationError
from empreses.models import Cooperativa
from .models import FacturaEmesa, Trimestre, FacturaRebuda, LiquidacioTrimestre
from projectes.models import ProjecteAutoocupat


def validate_nif(nif):
    if len(nif) != 9:
        raise ValidationError(
            "El NIF consta de 9 cifras"
            )

    # Código de control para DNIs y NIEs
    nif = nif.upper()
    corr_dict = dict(zip(
        range(23),
        ["T", "R", "W", "A", "G", "M", "Y",
         "F", "P", "D", "X", "B", "N", "J",
         "Z", "S", "Q", "V", "H", "L", "C",
         "K", "E"]
    ))
    
    # DNIs
    try:
        dividend = int(nif[:-1])
        mod = dividend % 23
        if nif[-1] == corr_dict[mod]:
            return
    # NIEs
    except:
        if nif[0] == "X":
            dividend = int(nif[:-1].replace("X", "0"))
        elif nif[0] == "Y":
            dividend = int(nif[:-1].replace("Y", "1"))
        elif nif[0] == "Z":
            dividend = int(nif[:-1].replace("Z", "2"))
        try: 
            mod = dividend % 23
            if nif[-1] == corr_dict[mod]:
                return
        except:
            pass
    
    # Código de control para otros NIFs
    digits = nif[1:-1]
    even_ds = digits[1::2]
    odd_ds = digits[::2]
    sum_evens = sum((int(n) for n in even_ds))
    sum_odds = 0

    for n in odd_ds:
        sum_odds += sum((int(q) for q in str(int(n) * 2)))
    sum_of_sums = sum_evens + sum_odds
    last_digit = int(str(sum_of_sums)[-1])
    control_result = last_digit if last_digit == 0 else 10 - last_digit

    try:
        control_code = int(nif[-1])
        if control_code == control_result:
            return        
    except:
        corr_dict = dict(zip(
            range(10),
            ["J", "A", "B", "C", "D", "E", "F", "G", "H", "I"],
        ))
        control_code = nif[-1]
        if control_code == corr_dict[control_result]:
            return
    raise ValidationError(
        "El Código de Control no coincide"
    )


class FormulariFiltrarLlistatAportacionsTrimestre(forms.Form):

    trimestre = forms.ModelChoiceField(
        queryset=Trimestre.objects.filter(obert=False),
        empty_label=None,
        label=u"Trimestre a considerar",
    )
    
    
class ExportFeesCsvForm(forms.Form):

    trimestre = forms.ModelChoiceField(
        queryset=Trimestre.objects.filter(obert=False),
        empty_label=None,
        label=u"Trimestre a considerar",
    )


class ExportInvoicesCsvForm(forms.Form):

    FACTURES_REBUDES = 'rebudes'
    FACTURES_EMESES = 'emeses'
    FACTURES_CHOICES = (
        (FACTURES_REBUDES, u"Factures Rebudes"),
        (FACTURES_EMESES, u"Factures Emeses"),
    )
    trimestre = forms.ModelChoiceField(
        queryset=Trimestre.objects.all(),
        empty_label=None,
        label=u"Trimestre a considerar",
    )
    tipus_factures = forms.ChoiceField(choices=FACTURES_CHOICES, label=u"Tipus de factures")


class FormulariImportExtracteBancari(forms.Form):

    file = forms.FileField()


class ModificarEmpresaForm(forms.Form):
    nif = forms.CharField(max_length=64, required=True, label="NIF")
    nom_fiscal = forms.CharField(max_length=256, required=True, label="Nom Fiscal")
    telefon = forms.CharField(max_length=64, required=True, label="Telefon")
    email = forms.EmailField(required=True, label="Email")
    adreca_fiscal = forms.CharField(max_length=256, required=True, label=u"Adreça fiscal")
    poblacio = forms.CharField(max_length=128, required=True, label=u"Població")
    codi_postal = forms.CharField(max_length=16, required=True, label=u"Codi Postal")

    
class CrearEmpresaForm(forms.Form):

    nom_fiscal = forms.CharField(max_length=256, required=True, label="Nom Fiscal")
    telefon = forms.CharField(max_length=64, required=True, label="Telefon")
    email = forms.EmailField(required=True, label="Email")
    adreca_fiscal = forms.CharField(max_length=256, required=True, label=u"Adreça fiscal")
    poblacio = forms.CharField(max_length=128, required=True, label=u"Població")
    codi_postal = forms.CharField(max_length=16, required=True, label=u"Codi Postal")

    
class BuscarEmpresaForm(forms.Form):

    nif = forms.CharField(max_length=64, required=True, label="NIF", validators=[validate_nif ])


class FormSeleccioVolumFacturacio(forms.Form):

    cooperativa = forms.ChoiceField(required=True)
    periode = forms.ChoiceField(required=True)

    def __init__(self, *args, **kwargs):

        trimestres = Trimestre.objects.all().order_by('data_inici')
        y = trimestres[0].data_inici.year
        periodes = [(str(y), str(y))]  # first choice is the first year in the records
        for t in trimestres:
            if y != t.data_inici.year:
                # the first time we see a different year
                y = t.data_inici.year
                periodes.append((str(y), str(y)))  # next choice is that year

            periodes.append((t.nom, t.nom))

        periodes = (x for x in periodes)

        llista_cooperatives = Cooperativa.objects.all().order_by('nom_fiscal').values_list('id', 'nom_fiscal')

        attrs = dict(onchange='document.forms[0].submit()')

        super(FormSeleccioVolumFacturacio, self).__init__(*args, **kwargs)
        self.fields['periode'].choices = periodes
        self.fields['periode'].widget.attrs = attrs
        self.fields['cooperativa'].choices = llista_cooperatives
        self.fields['cooperativa'].widget.attrs = attrs


class FormCercarValidacioFacturaRebuda(forms.Form):

    OPCIO_DATA = 'data'
    OPCIO_PROJECTE = 'projecte'
    OPCIO_PROVEIDOR = 'proveidor'

    OPCIONS = (
        (OPCIO_DATA, u"Data de la factura"),
        (OPCIO_PROJECTE, u"Projecte autoocupat que ha rebut la factura"),
        (OPCIO_PROVEIDOR, u"Proveïdor que ha emès la factura")
    )

    cooperativa = forms.ChoiceField(required=False)
    coop = forms.ChoiceField(required=False)
    ordenar_per = forms.ChoiceField(required=False, choices=OPCIONS)
    mostrar_estat = forms.ChoiceField(required=False, choices=FacturaRebuda.ESTATS, label=u"Estat")
    limit = forms.IntegerField(required=False, min_value=5, max_value=150, label=u"Mostrar com a màxim")
    des_de = forms.DateField(required=False)
    fins_a = forms.DateField(required=False)

    def __init__(self, *args, **kwargs):

        llista_cooperatives = Cooperativa.objects.all().order_by('nom_fiscal').values_list('id', 'nom_fiscal')
        llista_coops = [('', 'tots')] + list(ProjecteAutoocupat.objects.all().order_by('compte_ces_assignat').values_list('id', 'compte_ces_assignat'))
        super(FormCercarValidacioFacturaRebuda, self).__init__(*args, **kwargs)
        self.fields['cooperativa'].choices = llista_cooperatives
        self.fields['coop'].choices = llista_coops


class FormValidacioFacturaRebuda(forms.ModelForm):

    class Meta:
        model = FacturaRebuda
        fields = ['estat', 'vist_i_plau_observacions']
        widgets = dict(vist_i_plau_observacions=forms.TextInput)

    def __init__(self, *args, **kwargs):
        super(FormValidacioFacturaRebuda, self).__init__(*args, **kwargs)
        self.fields['estat'].widget.attrs = {'class': 'do-not-selectize', 'style': 'display:none'}
        self.fields['vist_i_plau_observacions'].widget.attrs = {'onblur': 'amagar_observacions(event)'}


class FormCrearFacturaEmesa(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        clients = kwargs.pop('clients')
        super(FormCrearFacturaEmesa, self).__init__(*args, **kwargs)
        self.fields['client'].queryset = clients

    class Meta:
        model = FacturaEmesa
        fields = ['client', 'data', 'observacions']

    def clean_data(self):

        data = self.cleaned_data['data']

        if not Trimestre.objects.filter(obert=True, data_inici__lte=data, data_final__gte=data).exists():
            raise forms.ValidationError(u"La data ha d'estar dintre d'un dels trimestres oberts")

        return data


class FormCrearFacturaRebuda(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        proveidors = kwargs.pop('proveidors')
        super(FormCrearFacturaRebuda, self).__init__(*args, **kwargs)
        self.fields['proveidor'].queryset = proveidors

    class Meta:
        model = FacturaRebuda
        fields = ['proveidor', 'data', 'numero']

    def clean_data(self):

        data = self.cleaned_data['data']

        if not Trimestre.objects.filter(obert=True, data_inici__lte=data, data_final__gte=data).exists():
            raise forms.ValidationError(u"La data ha d'estar dintre d'un dels trimestres oberts")

        return data


class FormConfirmarFacturaEmesa(forms.ModelForm):

    class Meta:
        model = FacturaEmesa
        fields = [
            'data_venciment',
            'forma_de_pagament',
        ]


class FormEditarFacturaRebuda(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(FormEditarFacturaRebuda, self).__init__(*args, **kwargs)
        self.fields['tipus_irpf'].widget = forms.TextInput(attrs={
            'onchange': 'actualitzar_totals()',
            'class': 'text-right'})
        self.fields['tipus_irpf'].required = False

    class Meta:
        model = FacturaRebuda
        fields = ['tipus_irpf', ]


class FormConciliacioAutomaticaMovimentExtracteBancariFactura(forms.Form):

    comentaris = forms.CharField(required=False)
    conciliacio_automatica_observacions = forms.CharField(required=False)
    moviment_extracte_id = forms.IntegerField()
    factura_id = forms.IntegerField()


class FormDescartarMovimentExtracteBancari(forms.Form):

    comentaris = forms.CharField(required=False)
    moviment_extracte_id = forms.IntegerField()


class FormLiquidacioTrimestre(forms.Form):
    projecte_id = forms.IntegerField(widget=forms.HiddenInput())
    trimestre_id = forms.IntegerField(widget=forms.HiddenInput())


class FormFiltreLiquidacioLlistatProjectes(forms.Form):
    estat = forms.ChoiceField()

    def __init__(self, *args, **kwargs):
        estat_choices = tuple((x[0], x[1]+u"s") for x in LiquidacioTrimestre.ESTATS)  # dirty trick in a hurry ;)
        estat_choices = (('_tots', u"(tots)"),) + estat_choices                       # it's time of dirty tricks :(
        super(FormFiltreLiquidacioLlistatProjectes, self).__init__(*args, **kwargs)
        self.fields['estat'].choices = estat_choices
        self.fields['estat'].widget.attrs = dict(onchange='filtre_estat_changed(event)')
