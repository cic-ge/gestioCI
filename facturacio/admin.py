from django.contrib import admin

from .models import FacturaEmesa, FacturaRebuda, LiniaFactura, Trimestre, TarifaQuotaTrimestral, \
                    ExtracteBancari, MovimentExtracteBancari

for model in (FacturaEmesa,
              FacturaRebuda,
              LiniaFactura,
              Trimestre,
              TarifaQuotaTrimestral,
              ExtracteBancari,
              MovimentExtracteBancari):

    admin.site.register(model)
