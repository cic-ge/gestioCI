# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('facturacio', '0005_moviments_i_balanc'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExtracteBancari',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom_arxiu', models.CharField(max_length=128)),
                ('data_importacio', models.DateTimeField(auto_now_add=True)),
                ('usuaria_importadora', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='MovimentExtracteBancari',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('compte', models.CharField(max_length=64)),
                ('tipus', models.CharField(max_length=64)),
                ('data', models.DateField()),
                ('concepte', models.CharField(max_length=128)),
                ('quantitat', models.DecimalField(max_digits=8, decimal_places=2)),
                ('compte_projecte', models.CharField(max_length=16, null=True)),
                ('numero_factura', models.CharField(max_length=64, null=True)),
                ('comentaris', models.TextField(max_length=256, null=True)),
                ('extracte_bancari', models.ForeignKey(to='facturacio.ExtracteBancari')),
            ],
        ),
    ]
