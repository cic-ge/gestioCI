# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0009_movimentextractebancari_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movimentextractebancari',
            name='comentaris',
            field=models.TextField(default='', max_length=256, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='movimentextractebancari',
            name='compte_projecte',
            field=models.CharField(default='', max_length=16, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='movimentextractebancari',
            name='numero_factura',
            field=models.CharField(default='', max_length=64, blank=True),
            preserve_default=False,
        ),
    ]
