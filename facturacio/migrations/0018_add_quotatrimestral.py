# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projectes', '0003_auto_20160213_1257'),
        ('facturacio', '0017_rename_quotatrimestral_to_tarifaquotatrimestral'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuotaTrimestral',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('base_imposable', models.DecimalField(max_digits=8, decimal_places=2)),
                ('import_quota', models.DecimalField(max_digits=8, decimal_places=2)),
                ('estat', models.CharField(default=b'emesa', max_length=16, choices=[(b'emesa', 'Pendent de cobrar'), (b'cobrada', 'Cobrada')])),
                ('moviment', models.ForeignKey(to='facturacio.Moviment')),
                ('projecte', models.ForeignKey(related_name='+', to='projectes.ProjecteAutoocupat')),
                ('trimestre', models.ForeignKey(related_name='+', to='facturacio.Trimestre')),
            ],
            options={
                'verbose_name': 'Quota trimestral',
                'verbose_name_plural': 'Quotes trimestrals',
            },
        ),
        migrations.AlterUniqueTogether(
            name='quotatrimestral',
            unique_together=set([('trimestre', 'projecte')]),
        ),
    ]
