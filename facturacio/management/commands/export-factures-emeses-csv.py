# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from facturacio.models import FacturaEmesa, LiniaFactura

import codecs


class Command(BaseCommand):
    help = 'Export to csv all FacturaEmesa with an invoice number set'

    def handle(self, *args, **options):

        fes = FacturaEmesa.objects.exclude(numero=None)
        
        with codecs.open("gci-factures-emeses-2016.csv", "w", encoding="utf-8") as f:
            f.write("num_socia$cif_client$cif_proveidor$nom_client$nom_proveidor$num_factura$data$base_imponible$tipus_iva$iva_parcial$perc_req$req$total_factura")
            f.write("\n")
            for fe in fes:
                iva_tuples = [(sum([l.base_total for l in fe.liniafactura_set.filter(tipus_iva=tipus)]), tipus, sum([l.iva_total for l in fe.liniafactura_set.filter(tipus_iva=tipus)])) for tipus in [LiniaFactura.IVA_0, LiniaFactura.IVA_4, LiniaFactura.IVA_10, LiniaFactura.IVA_21]]
                try: 
                    perc_req = fe.req_total * 100 / fe.base_total
                except:
                    perc_req = 0
                for t in iva_tuples:
                    f.write(fe.projecte.compte_ces_assignat+"$"+fe.client.nif+"$"+fe.proveidor.nif+"$"+fe.client.nom_fiscal+"$"+fe.proveidor.nom_fiscal+"$"+fe.numero+"$"+str(fe.data)+"$"+str(t[0])+"$"+str(t[1])+"$"+str(t[2])+"$"+str(perc_req)+"$"+str(fe.req_total)+"$"+str(fe.import_total))
                    f.write("\n")


