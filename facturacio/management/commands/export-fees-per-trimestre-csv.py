from django.core.management.base import BaseCommand

from facturacio.models import Trimestre, ProjecteAutoocupat, QuotaTrimestral, Moviment

import codecs

class Command(BaseCommand):
    help = "Export to CSV list of fees per Trimestre object"

    def add_arguments(self, parser):
        parser.add_argument('year', type=int)
        parser.add_argument('num', type=int)

    def handle(self, *args, **options):

        trimestre = Trimestre.trobar_per_any_i_numero(options['year'], options['num'])

        with codecs.open("llistat_aportacions_trimestre_"+trimestre.nom+".csv", "w", "utf-8") as f:

            f.write("coop$nom_i_cognoms$rao_social$aportacio_cic$aportacio_iva")
            f.write("\n")
            for projecte in ProjecteAutoocupat.objects.all():

                try:

                    quota_trimestral = QuotaTrimestral.objects.get(projecte=projecte, trimestre=trimestre)
                    
                    moviment_iva = [moviment for moviment in Moviment.objects.filter(projecte=projecte) if "IVA" in moviment.concepte and trimestre.nom in moviment.concepte][0]

                    f.write(projecte.compte_ces_assignat+"$"+projecte.membres_de_referencia.all()[0].nom_sencer_sense_email+"$"+projecte.cooperativa_assignada.nom_fiscal+"$"+str(quota_trimestral.import_quota)+"$"+str(moviment_iva.quantitat)+"\n")

                except:
                    
                    self.stdout.write("El projecte %s no te moviments per el trimestre %s" % (projecte.compte_ces_assignat, trimestre.nom))
                    continue


