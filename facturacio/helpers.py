# coding=utf-8
import re

from calendar import monthrange
from datetime import date

from decimal import Decimal

from django.db import transaction
from django.db.models import Max, Sum

from gestioci.helpers import now
from gestioci.settings import auth_groups


def is_user_responsable_facturacio(user):
    """
    function that returns True when user belongs
    to RESPONSABLES_FACTURACIO group
    """
    from gestioci.settings import auth_groups
    return user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists()


def is_user_responsable_bancari(user):
    """
    function that returns True when user belongs
    to RESPONSABLES_BANCARIS group
    """
    from gestioci.settings import auth_groups
    return user.groups.filter(name=auth_groups.RESPONSABLES_BANCARIS).exists()


def trobar_projectes_usuari(user):
    """
    :param user:
    :return: queryset object with the ProjecteAutoocupat objects where
     the auth.User object is [indirectly through PersonaUsuaria] a
     membre de referència
    """
    from socies.models import PersonaUsuaria
    from projectes.models import ProjecteAutoocupat

    try:
        persona = PersonaUsuaria.objects.get(usuari=user).persona
        resultat = persona.projectes_autoocupats.all()
    except PersonaUsuaria.DoesNotExist:
        resultat = ProjecteAutoocupat.objects.none()

    return resultat


def trobar_balanc_actual(projecte):
    """
    :param projecte:
    :return: dict amb:
                - últim trimestre tancat (or None)
                - balanç al tancament de l'últim trimestre (or None)
                - trimestre a tancar (or None)
                - estat del trimestre a tancar
                - balanç al tancament del trimestre a tancar (or None)
                - balanç a data d'avui
    """

    from facturacio.models import LiquidacioTrimestre, Moviment

    zero = Decimal('0.00')

    balanc_inicial = zero
    balanc_ultim_trimestre_tancat = zero
    balanc_trimestre_a_tancar = None
    primer_dia_a_considerar = date(1970, 1, 1)

    ultim_trimestre_tancat = trobar_ultim_trimestre_tancat()  # tancat de veritat, no parlem de liquidacions aquí
    trimestre_a_tancar = trobar_primer_trimestre_obert()      # tancat de veritat, no parlem de liquidacions aquí
    estat_trimestre_a_tancar = LiquidacioTrimestre.ESTAT_OBERT  # assumim que encara no s'ha [auto]liquidat

    if ultim_trimestre_tancat:
        primer_dia_a_considerar = trimestre_a_tancar.data_inici

        # busquem la liquidació de l'últim trimestre tancat de veritat, no parlem d'[auto]liquidació aquí
        liquidacio = LiquidacioTrimestre.objects.filter(projecte=projecte, trimestre=ultim_trimestre_tancat)
        if liquidacio.exists():
            balanc_ultim_trimestre_tancat = liquidacio.values_list('balanc_final', flat=True)[0]
            balanc_inicial = balanc_ultim_trimestre_tancat

    # busquem la liquidació del trimestre a tancar, per saber si està [auto]liquidat
    liquidacio = LiquidacioTrimestre.objects.filter(projecte=projecte, trimestre=trimestre_a_tancar)
    if liquidacio.exists():
        estat_trimestre_a_tancar = liquidacio[0].estat

    moviments = Moviment.objects.filter(projecte=projecte).filter(data__gte=primer_dia_a_considerar)
    import_acumulat_fins_avui = moviments.aggregate(Sum('quantitat')).get('quantitat__sum') or zero
    balanc_actual = balanc_inicial + import_acumulat_fins_avui

    if trimestre_a_tancar:
        moviments = moviments.filter(data__lte=trimestre_a_tancar.data_final)
        import_fins_tancament_trimestre_a_tancar = moviments.aggregate(Sum('quantitat')).get('quantitat__sum') or zero
        balanc_trimestre_a_tancar = balanc_inicial + import_fins_tancament_trimestre_a_tancar

    return dict(
        ultim_trimestre_tancat=ultim_trimestre_tancat,
        balanc_ultim_trimestre_tancat=balanc_ultim_trimestre_tancat,
        trimestre_a_tancar=trimestre_a_tancar,
        estat_trimestre_a_tancar=estat_trimestre_a_tancar,
        balanc_trimestre_a_tancar=balanc_trimestre_a_tancar,
        balanc_actual=balanc_actual,
    )


re_trimestre = re.compile(r'\d{4}T[1-4]\Z', re.IGNORECASE)  # e.g. 2015T2
re_any = re.compile(r'\d{4}\Z')  # e.g. 2015


def calcular_periode(spec):
    """
    Retorna dues dates (de calendari, sense rellotge) que corresponen a l'inici i final del periode;
    els periodes es poden especificar:
    - amb el nom d'un trimestre, p.ex. 2015T2
    - amb un any, p.ex. 2015
    """
    if re_trimestre.match(spec):
        from facturacio.models import Trimestre
        try:
            trimestre = Trimestre.objects.get(nom=spec.upper())
            return trimestre.data_inici, trimestre.data_final
        except Trimestre.DoesNotExist:
            return None, None

    if re_any.match(spec):
        year = int(spec)
        if year > 1900:
            return date(year, 1, 1), date(year, 12, 31)

    return None, None


def calcular_trimestre(data):
    """
    Retorna (any, trimestre, inici, final) per al trimestre que inclou la data especificada.
    Trimestre és un enter d'aquests: [1, 2, 3, 4]
    """
    year = data.year
    trimestre = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][data.month-1]
    primer_mes = (trimestre - 1) * 3 + 1
    ultim_mes = primer_mes + 2
    inici = date(year, primer_mes, 1)
    final = date(year, ultim_mes, monthrange(year, ultim_mes)[1])
    return year, trimestre, inici, final


def trobar_base_imposable_i_iva(projecte, trimestre):
    """
    Suma la base imposable de totes les factures emeses i resta la base imposable de totes les factures rebudes.
    Addicionalment retorna els dos valors i el recompte de factures d'un i altre tipus
    :param projecte:
    :param trimestre:
    :return: dict amb:
              - base imposable resultant (resultat de la resta d'emeses menys rebudes)
              - iva resultant (resultat de la resta d'emeses menys rebudes)
              - recompte de les factures emeses
              - base imposable de les factures emeses
              - iva de les factures emeses
              - recompte de les factures rebudes
              - base imposable de les factures rebudes
              - iva de les factures rebudes
    """
    from facturacio.models import FacturaEmesa, FacturaRebuda

    inici = trimestre.data_inici
    final = trimestre.data_final

    factures_emeses = FacturaEmesa.objects.filter(projecte=projecte, data__gte=inici, data__lte=final,).exclude(numero=None)
    factures_rebudes = FacturaRebuda.objects.filter(projecte=projecte, data__gte=inici, data__lte=final, estat=FacturaRebuda.ESTAT_ACCEPTADA)

    emeses_base = Decimal('0.00')
    rebudes_base = Decimal('0.00')
    emeses_iva = Decimal('0.00')
    rebudes_iva = Decimal('0.00')

    emeses_count = factures_emeses.count()
    if emeses_count:
        emeses_base = factures_emeses.aggregate(Sum('base_total'))['base_total__sum']
        emeses_iva = factures_emeses.aggregate(Sum('iva_total'))['iva_total__sum']

    rebudes_count = factures_rebudes.count()
    if rebudes_count:
        rebudes_base = factures_rebudes.aggregate(Sum('base_total'))['base_total__sum']
        rebudes_iva = factures_rebudes.aggregate(Sum('iva_total'))['iva_total__sum']

    base = emeses_base - rebudes_base
    iva = emeses_iva - rebudes_iva

    return dict(base=base,
                iva=iva,
                emeses_count=emeses_count,
                emeses_base=emeses_base,
                emeses_iva=emeses_iva,
                rebudes_count=rebudes_count,
                rebudes_base=rebudes_base,
                rebudes_iva=rebudes_iva,
                )


def trobar_quota_trimestral(data, base_imposable_rebuda, base_imposable_emesa):

    from facturacio.models import TarifaQuotaTrimestral

    # la taula de quotes que aplica és la que té la data d'entrada en vigor màxima tal que
    # la data d'entrada en vigor és menor o igual a la data en que es vol fer el moviment
    data_vigor = TarifaQuotaTrimestral.objects.filter(data_vigor__lte=data).aggregate(Max('data_vigor'))['data_vigor__max']

    if data_vigor is None:
        raise ValueError(u"No hi ha quotes trimestrals definides per aquesta data (%s)" % data.isoformat())

    # donada aquesta data d'entrada en vigor, trobem el tram de facturació i la quota trimestral que aplica:
    # de tots els trams per imports inferiors o iguals a la facturació que s'indica, ens quedem amb el que
    # correspon a l'import més gran
    quota_taula = TarifaQuotaTrimestral.objects.filter(data_vigor=data_vigor)
    quota_taula = quota_taula.filter(base_imposable_minima__lte=max(Decimal('0.00'), base_imposable_emesa))
    quota_taula = quota_taula.order_by('-base_imposable_minima')

    quota_taula = list(quota_taula)

    if len(quota_taula):
        quota_taula = quota_taula[0].import_quota_trimestral
    else:
        raise ValueError(u"Manca un tram amb 'base imposable minima' = 0 a 'quota trimestral' "
                         u"per 'data d'entrada en vigor' = %s" % data_vigor)

    # TODO: treure aquesta constant "0.15" a una taula amb data_vigor etc. anàloga a TarifaQuotaTrimestral
    quota_percentatge = max(Decimal('0.00'), base_imposable_emesa - base_imposable_rebuda) * Decimal('0.12')
    quota_percentatge = quota_percentatge.quantize(Decimal('0.01'))

    return max(quota_percentatge, quota_taula)


def trobar_primer_trimestre_obert():
    """
    Retorna el Trimestre obert més antic (o None si no n'hi ha cap d'obert xD)
    """
    from facturacio.models import Trimestre
    q = Trimestre.objects.filter(obert=True).order_by('data_inici')
    if q.exists():
        return q[0]

    return None


def trobar_ultim_trimestre_tancat():
    """
    Retorna el Trimestre que s'ha tancat més recentment, o None si no n'hi ha cap de tancat
    """
    from facturacio.models import Trimestre
    q = Trimestre.objects.filter(obert=False).order_by('-data_limit_tancament')
    if q.exists():
        return q[0]

    return None


def trobar_o_crear_liquidaciotrimestre(projecte, trimestre):

    from .models import LiquidacioTrimestre

    try:
        liquidacio = LiquidacioTrimestre.objects.get(projecte=projecte, trimestre=trimestre)

    except LiquidacioTrimestre.DoesNotExist:
        liquidacio = LiquidacioTrimestre(projecte=projecte, trimestre=trimestre)

    return liquidacio


@transaction.atomic
def tancar_trimestre_projecte(projecte, trimestre_a_tancar, ultim_trimestre_tancat, usuari):

    if not usuari.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
        raise

    liquidacio = trobar_o_crear_liquidaciotrimestre(projecte, trimestre_a_tancar)
    if liquidacio.estat != liquidacio.ESTAT_TANCAT:

        bal = trobar_balanc_actual(projecte)

        assert ultim_trimestre_tancat == bal['ultim_trimestre_tancat']
        assert trimestre_a_tancar == bal['trimestre_a_tancar']

        liquidacio.balanc_final = trobar_balanc_actual(projecte)['balanc_trimestre_a_tancar']
        liquidacio.data_liquidacio = now()
        liquidacio.usuari_liquidacio = usuari
        liquidacio.save()

        resum_facturacio = trobar_base_imposable_i_iva(projecte, trimestre_a_tancar)

        import_quota = trobar_quota_trimestral(trimestre_a_tancar.data_limit_tancament,
                                               resum_facturacio['rebudes_base'],
                                               resum_facturacio['emeses_base'])

        import_iva = resum_facturacio['iva'] if resum_facturacio['iva'] > Decimal('0.00') else Decimal('0.00')

        # TODO ens demanen que no creem un moviment, que fem un objecte de tipus "Quota" similar a FacturaEmesa al
        # costat contrari de Moviment <-> MovimentExtracteBancari
        # TODO jo penso que es millor així (i també és més simple de programar, tot i que ja vaig fer una bona part!
        # i també és més intuitiu per les sòcies!)

        from .models import Moviment, QuotaTrimestral

        moviment = Moviment()
        moviment.projecte = projecte
        moviment.cooperativa = projecte.cooperativa_assignada
        moviment.concepte = u"Aportació trimestral %s" % trimestre_a_tancar
        moviment.quantitat = import_quota
        moviment.data = now()
        moviment.created_by = usuari
        moviment.updated_by = usuari
        moviment.save()

        moviment = Moviment()
        moviment.projecte = projecte
        moviment.cooperativa = projecte.cooperativa_assignada
        moviment.concepte = u"Aportació IVA %s" % trimestre_a_tancar
        moviment.quantitat = import_iva
        moviment.data = now()
        moviment.created_by = usuari
        moviment.updated_by = usuari
        moviment.save()

        QuotaTrimestral.objects.create(
            projecte=projecte,
            trimestre=trimestre_a_tancar,
            base_imposable_emesa=resum_facturacio['emeses_base'],
            base_imposable_rebuda=resum_facturacio['rebudes_base'],
            import_quota=import_quota,
            moviment=moviment,
        )

    return liquidacio
