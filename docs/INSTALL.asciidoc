= Deploying GestioCI

[NOTE]
This guide shows examples based on _Debian 7 Wheezy_; In particular, the official Debian 7 openvz template provided by Proxmox VE 3.x.


== System pre-requisites

The ideal starting point is a clean installation of a GNU/Linux distribution (e.g. Debian, OpenSUSE, RedHat, ...).

Regarding Python, as a sane default, we will rely on the up-to-date version shipped with the distribution of:

* the _Python 2.7_ interpreter
* the +virtualenvwrapper+ package

Aside from these two packages (plus dependencies) we will try our best to isolate our application (_django project_) from the distribution's Python environment. As far as we are aware, once inside a _virtualenv_, we are only relying on the distribution's python interpreter. Any other python library we need, we will have to include it on our _virtualenv_. Some python libraries (e.g. +psycopg2+) have to be built (although +pip+ takes care of it) with the GNU +gcc+ toolchain plus additional development packages on the system (e.g. +libpq-dev+, +libjpeg-dev+, etc.).

[NOTE]
We try to develop, test and deploy to matching environments whenever possible. We might try _vagrant_ or _docker_ or whatever, but we might as well just try and get used to _Nix_/_NixOS_ for the same price; A much better investment, I believe.


=== Locale

We recommend setting up the system with a _UTF-8_ locale. The _PostgreSQL_ database cluster will -- by default -- be created with the system's global locale/encoding. If you choose a ``simpler'' locale when installing PostgreSQL, you might be unable to create UTF-8 databases later. In addition, we recommend that you choose an English locale; this will be helpful to have error messages in the native language when reaching for help.

[source,sh]
----
dpkg-reconfigure locales  # choose en_US.UTF8 as the system's default
----


=== System packages

As commented above, we need:

* the _gcc toolchain_
* the database server, _PostgresQL_, plus the development package
* the _virtualenv_ tools
* development packages for the libraries which are dependencies of the python packages (e.g. _Pillow_)

[source,sh]
----
apt-get install postgresql postgresql-client libpq-dev \
                libjpeg-dev libpng12-dev libfreetype6-dev \
                python-dev virtualenvwrapper gcc
----


=== Dedicated system user

We set up a dedicated system user for the python virtualenv and for running the application:

[source,sh]
----
useradd -m gci -s /bin/bash
su - gci
----


== Getting the application

Dump the application sources on an appropriate directory, e.g. +gci-produccio+, +gci-staging+, simply +gci+, or whatever.

An example for getting a working copy (_git clone_) directly from _GitLab_ follows. For production, we would grab a release tarball, check signatures, etc.; If checking out from the git repo for production, choose the appropriate tag name; you can then get rid of +.git/+ and +.gitignore+ on the fetched working copy. We will, eventually, package and sign releases, though.

[source,sh]
----
git clone https://gitlab.com/cooperativa-integral-catalana/gestioCI.git gci
cd gci
----


== Python _virtualenv_

Create a _virtualenv_ for the application instance; We suggest that you match the name with that of the directory where the application has been deployed.

[source,sh]
----
mkvirtualenv --no-site-packages gci  # --no-site-packages is now the default behaviour
workon gci  # recent versions of virtualenvwrapper already drop you inside after mkvirtualenv
echo 'workon gci' >> ~/.bashrc  # login directly to the virtualenv
----

WARNING: We have to ensure that the virtualenv is always active *before running any application-specific python code*. This includes *all* invocations to +manage.py+; including of course the +runserver+ command. Also you have to take this in consideration when running via _WSGI_ or similar.

.Populating our _python virtualenv_
[source,sh]
----
pip install --upgrade pip
pip install --upgrade -r requirements/production  # choose the appropriate requirements file
----

WARNING: Please pay attention at the messy and horribly ultra-verbose output of the +pip install+ command! There might be clues of missing system libraries that get the python libraries potentially built without support for certain required features (e.g. _Pillow_ with no support for _jpg_ or _png_). This applies to dependencies also: when the `broken' libraries are not explicitly listed on the _requirements file_, they won't be rebuilt by invoking +pip+ again. +pip+ will consider them already satisfied.


== Application configuration

We have a split django +settings.py+ setup with separate configuration files for the different environments where we usually deploy the application:

[source,sh]
----
gci/
     settings/
               base.py
               development.py
               staging.py
               production.py
----

There is some configuration stuff that is common to every environment; it goes in +base.py+.

All the other files inherit from +base.py+ (this is actually a python import). In addition, the *staging* settings module _imports_ the *production* settings module. This is to keep the testing environment as close as possible to the production environment.

So you have to override settings down the settings hierarchy, as close to the +base.py+ root as possible:

* +base.py+ -> +development.py+
* +base.py+ -> +production.py+ -> +staging.py+

For example, if you are using the same SMTP host for _staging_ and _production_, you would define it in +base.py+ and override it in +development.py+. If you prefer to not share the SMTP host settings with your developers, you can remove these settings (or leave the example values) in +base.py+ and override them in +production.py+.

.Environment variable to be added, to e.g. +~/.bashrc+
[source,sh]
----
export DJANGO_SETTINGS_MODULE='gestioci.settings.development'  # choose the appropriate settings module
----


=== Secrets

We don't store secrets of any kind (passwords, keys or seeds) on the application's config files. All secrets needed by the application are retrieved from environment variables. So you have to set some environment before launching the server. Please take this in consideration when running via _WSGI_ or similar.

In addition, the appropriate configuration file must be pointed at by the +DJANGO_SETTINGS_MODULE+ environment variable. And it has to be specified in the python package dotted format:

.Environment variables to be added, to e.g. +~/.bashrc+
[source,sh]
----
export GCI_SECRET_KEY='xxxxxxxxxxxxxxx'  # you have to generate a long random string, and keep it secret
export GCI_EMAIL_HOST_PASSWORD='yyyyyy'  # this is the password for logging to the SMTP server
----


=== Database

We match the _database user_ name with the _system's user_ name, so the default PostgreSQL authentication configuration allows passwordless connections. If you choose otherwise, or to have the database on a different host, you might need additional stuff here (e.g. +HOSTNAME+). Also, as we don't like secrets on our config files, you can consider adding another environment variable, or maybe the +~/.pgpass+ file sounds good to you. This example assumes a PostgreSQL instance running on the same server. For more complex examples please refer to the django documentation (see the comment below).

[source,python]
----
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME':   'gci',
        'USER':   'gci',
    }
}
----


=== Email settings

The application needs to send email for different purposes (e.g. from the ``password reset'' facility). The SMTP host and account settings, *except for the password*, are as follows:

[source,python]
----
SMTP_HOST = 'smtp.example.com'
SMTP_PORT = 587
SMTP_USE_TLS = True
SMTP_USERNAME = 'no-reply'
SMTP_DEFAULT_SENDER = 'no-reply@example.com'
----


=== Webserver hostname

When the debugging features are disabled, e.g. in the production environment, we have to inform _django_ about its ``real server hostname'', that is how it will be called from the Real World:

[source,python]
----
ALLOWED_HOSTS = [ 'gestio.example.com', ]
BASE_URL = 'https://gestio.example.com'  # no trailing slash please
----


=== Static files

_Static files_ are files such as images, CSS, JavaScript, and any other kind of document that is not _dynamically generated_ by the application (hence the name _static_). Such documents are usually not served by _django_ but by the <<front-end-webserver,_front-end web server_>>, e.g. _apache_ or _nginx_. 

[NOTE]
Files uploaded by the user are not considered part of this _static_ category, even though -- once uploaded -- they are indeed static. But they have its own category (see <<media-files,_Media files_>> below).

Django has a way to ``collect'' all those files from the application directory _and_ the python packages, and copy them in a single place. This simplifies the front-end web server configuration.

* +STATIC_URL+ is the prefix for the URLs that point to _static_ content.
* +STATICFILES_DIRS+ is a collection of places where static files are to be collected. This is in addition to static files defined by each _Django app_.
* +STATIC_ROOT+ is the location in the filesystem where the collected files are copied. The web server will serve this location on its own, so it doesn't have to be inside the application directory.

[source,python]
----
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'gestioci/static/build/'),
)
----

Specific to our application, there is a build process that leaves a bunch of static files in +gestioci/static/build/+.
TODO: document build-time dependencies, the build process itself, question if we should ship these static files already built. Same thing could apply to compiling asciidoc docs.

Once this is ready, you have to actually populate the +STATIC_ROOT+:

[source,bash]
----
python manage.py collectstatic
----


[[media-files]]
=== Media files

There are two kinds of files in the _media_ category:

* Files uploaded by the user.
* Documents generated by the application, that become ``static'' when stored on the filesystem.

These files are stored on the django's +MEDIA_ROOT+ directory. Each ``model'' defines its own directory inside that root, for example +FotoProjecteAutoocupatFiraire+ is uploading to +<MEDIA_ROOT>/fotos_parada_firaire+. And it is also generating thumbnails for the uploaded pictures under +<MEDIA_ROOT>/fotos_parada_firaire/.miniatures+.

+MEDIA_URL+ is the prefix for the URLs that point to _media_ content.
+MEDIA_ROOT+ is the location in the filesystem where the uploaded or generated files are stored. The web server will serve this location on its own, so it doesn't have to be inside the application directory.

[source,python]
----
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
----


== Creating a new database

NOTE: We use and recommend *PostgreSQL*. If you choose otherwise, you'll have to replace +psycopg2+ on the _requirements file_.

=== New database

Open a +psql+ console from the user +postgres+:

[source,sh]
----
su - postgres -c psql
----

Create a user and a database:

.Postgres commands
[source,sql]
----
CREATE USER gci WITH PASSWORD 'xxx'; -- BEWARE: this might end up stored in cleartext on psql's history file
-- in development environments you can ALTER USER gci WITH CREATEDB;
CREATE DATABASE gci OWNER gci;
\l
--                                   List of databases
--    Name    |  Owner   | Encoding |   Collate   |    Ctype    | ...
-- -----------+----------+----------+-------------+-------------+-----------------------
--  gci       | gci      | UTF8     | en_US.UTF-8 | en_US.UTF-8 | <--- LOOK HERE ;)
--  postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
--  template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
--  template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
-- (4 rows)
\q
----

WARNING: Please ensure the new database has been created with an appropriate _encoding_; We choose and recommend _UTF-8_. The default encoding depends upon the system's _locale_ (see above). If the _PostgreSQL_ database instance has been installed with a ``simpler'' locale, you'll want to change the system's locale and then re-create the database instance (+apt-get remove --purge postgresql && apt-get install postgresql+). You can review the database encoding with +psql -l+; Or you can issue the +\l+ command on the +psql+ prompt.


=== Loading up the schema

.You can check if _django_ has access to the database
[source,sh]
----
python manage.py dbshell  # if successful, you can exit with Control+D or \q
----

.Load up the database schema
[source,sh]
----
python manage.py migrate
----

You want a ``superuser'' that can create other users, assign them to roles, etc. In this application, the mapping between users and roles is done via django _auth groups_. The groups are created by the migrations.

.Create a ``superuser''
[source,sh]
----
python manage.py createsuperuser --username admin
----


=== Required initial data

Now we have to populate some choice fields; the _fixture_ on the next example corresponds to values that are specific to _Cooperativa Integal Catalana_; you may want to review if these values fit in your organization, or go ahead with or without loading them up and take care of this _later_ from the _django admin interface_:

[source,sh]
----
python manage.py loaddata cic_stuff/initial_data/socies-habilitats-contribucions.json
----

=== Create actual users

You have to create the _django user objects_ and associate them with roles (_django group objects_) as appropriate. So we suggest to also defer this task until you have access to the _django admin interface_.


== Updating an existing database

.You can check if _django_ has access to the database
[source,sh]
----
python manage.py dbshell  # if successful, you can exit with Control+D or \q
----

NOTE: Before attempting this procedure you might want to schedule a maintenance window, notify your users in advance, set up a _sorry page_, and *stop the running server*. It's probably a good idea to try the upgrade on a testing instance cloned from the production server.

.Dump the database so you can _roll back_ if something goes south
[source,sh]
----
pg_dump gci | xz > -o gci-YYYY-MM-DD-HH-MM.pg_dump.xz
----

.Apply the database migrations to bring the schema up to date
[source,sh]
----
python manage.py migrate
----

If something goes wrong, you can always restore the saved dump.
On the other hand, if all looks good, you can launch the application and run some validation tests.


== Cron job

(WIP)
TODO: ensure the environment variables are set
TODO: ensure the virtualenv is active before calling runcrons
TODO: set a proper destination for error reporting

[source,sh]
----
* * ...python manage.py runcrons
----


== Almost there

If you are deploying a development or staging instance, we have gone far enough as to evaluate or proceed to the initial setup with the application.

You should be able to launch django's web server and then browse into the application:

[source,sh]
----
python manage.py runserver
----

If you are deploying a production instance, you'll need to set up the front-end web server first, or cheat with your +/etc/hosts+ so you reach the application with the right host name. If you get a _400 Bad Request_, you are probably not reaching the application with the correct hostname (see +ALLOWED_HOSTS+ and +BASE_URL+ above).


[[front-end-webserver]]
== Front-end web server

(WIP) TODO: explain nginx and settle on a _WSGI_ strategy... _VS_ init scripts, etc.


== Backup strategy

WARNING: *You should have a proper _backup strategy_ in place _before_ your production instance goes live!* If a _database dump_ and a _backup policy_ sound equivalent to you, *you are compromising your _business continuity_, and that of your users*.

A full consideration of proper good practices for a backup strategy sounds off-topic for this document. But here are the things that should be considered when working it out, from more critical to less critical:

* User data
** A database dump.
** The contents of the +<MEDIA_ROOT>+ directory. It would possibly make sense to exclude the _generated thumbnails_, but we don't have (yet) an automatic way to regenerate them.
* Application
** The directory with the deployed application. A sane minimum would be the customized configuration files. Minus the +*.pyc+ python compiled modules. You can also spare the +STATIC_ROOT+ directory. TODO: perhaps the +gestioci/static/build/+ directory can also be spared?
** The file(s) where the environment variables are set up (+~/.bashrc+ in our examples).
* The application environment
** The system specification that allows you to rebuild the full server (it would be pretty lean in the case of NixOS). Otherwise you may consider to include:
*** The output of +dpkg -l+ or +rpm -qa+
*** The output of +pip freeze+, and/or perhaps the full _virtualenv_ (e.g. +~gci/.virtualenvs/gestioCI+)
*** The configuration of the front-end web server
*** And perhaps the relevant parts of the server's filesystems (e.g. excluding +/var/lib/postgresql/+ in favor of the database dump, etc.)

