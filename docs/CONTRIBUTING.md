Contributing Guide
==================

Acuerdos
--------

Antes de colaborar asegurate de haber leído nuestros consensos de desarrollo en docs/consensos.md .


¿Como recrear un entorno de desarrollo?
---------------------------------------

Sigue las instrucciones en docs/install.asciidoc


¿Como colaborar?
----------------

* Clona el repositorio
* Asígnate una tarea
* Prepara una Merge Request
* Añade tu nick en contributors.txt


Sobre el Frontend
-----------------

Leer `gestioci/static/README.md` para ver como trabajar con los CSS y Javascripts.
