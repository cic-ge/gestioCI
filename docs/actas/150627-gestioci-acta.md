Orden del día - Lunes 27 de Junio 2015
======================================

Propostes
---------

### Revisar release i veure si és publicable
- Revisar stage els canvis que hi ha fets directament allà

### resoldre migracions per fer la release
- fet!

### revisar export a csv
- manca COOP number al csv

### revisar blocking issues
- pendent de fer

### Revisar camí recorregut i camí a recorrer
molaria fer un document on es reculli:
- què s'ha fet i què manca
- valoracions
- previsió de la planificació
- replanificar tasques pendents

### COSES IMPORTANTS A FER
- pablo: reassignador de projectes
- duub: Revisar com s'està utilitzant git flow per fer releases

### Deploy on stage
- protocol a seguir, idees: branch feature/deploy extret de http://gitolite.com/deploy.html

### wsgi
he aconseguit fer anar uwsgi+nginx+django en local gràcies a aquest tutorial: https://uwsgi.readthedocs.org/en/latest/tutorials/Django_and_nginx.html#using-unix-sockets-instead-of-ports
hi ha alguns detallets que no explica, però en una tarda ho he pogut fer anar. Em proposo per mirar d'implementar-ho a stage.
revisar què aporta.

Scrum
-----

### Retrospectiva
### Demo
#### feature/change-password
* textos traduïts i branca merged a develop
### Planificació sprint 6
### Objectiu de l'esprint 6
### Seleccio llista d'histories
### Com demostrar-ho
### Histories divides en tasques
