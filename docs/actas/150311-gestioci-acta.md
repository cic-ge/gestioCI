Orden del día 15-03-11
======================

Informativos
------------

### Desarrollo CIC

* GestioCI
* Integral CES
    * Millores
* MercatCooperatiu
    * odoo?
    * cartdridge!
* LDAP

### Repaso semana anterior

#### Update refactor
- Falta una semana para acabar.

#### Update CSS
- Está acabado.

#### Update bugfixes


Propuestas
----------

### Hacer roadmap para versión 1.0

Los hitos podrian ser:

  - 0.4: Versión actual WIP
  - 0.5: Alta socies autoocupats terminado
  - 0.6: Facturación
  - 0.7: Alta socies cooperatives: http://alta.cooperativaintegral.cat es mover esto?
  - ...: Falta algo?
  - 1.0: Portal del socio

Estaria bien aclarar los features/issues que faltan para cerrar 0.5, ¿lo mirarmos en el Redmine?

Debates
-------

### Licencia [DONE]
- Tiraremos por AGPL3, obliga a quien de servicio usando GestioCI a compartir las modificaciones si las hace.
- Depositar los derechos en FSF; tendria sentido si realmente llegamos a tener algo.

### No usar los foros del Redmine, tirar de la lista de correo cicic-dev [DONE]
- Todos de acuerdo.

### Idioma del código catalan vs. english

### Como publicar el código, donde y como trabajar: GitLab/Gitourius vs Redmine
(posibilidad de monitoreo interno con redmine y seguimiento por parte de la cic vs no duplicar trabajo de publicacion etc etc...)
Publicar como: CIC, CICIC, CICIC-dev…?

### Git??
(debate pendiente)

Tareas
------

* Definir features proximos milestones: Facturación (prioriotario), Alta socies cooperatives, Portal del socio. [duub + pablo]?
