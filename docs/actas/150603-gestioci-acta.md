Orden del día - Miércoles 3 de Junio 2015
=========================================


Informativos
------------

### Lejanía efkin
entre el 20-21 y el 28 estaré en francia. no podré venir a las reus pero podré currar de lejos.

### Bug de javascript
és un bug de firefox que ha quedat resolt en firefox 33 i debian stable s'ha quedat a la 31.x fins que no s'actualitzi a la propera release de debian (stretch) que ja van per la 38


Debats
------

### Creació App Projecte
Es decideix crear app projecte (es mourà el que hi ha ara de alta_socies i posar-hi les classes de fotos allà

### Pas a producció
Ciclo de producción:
    * Crear una release cada dos semanas.
    * Lo que hay en release pasa a staging.


Scrum
-----

### Retrospectiva
- S'està dedicant més temps a pensar com implementar les diferents històries del que es preveia.

### Demo

#### feature/messages
Merged!

#### feature/datepicker
Merged!

### Planificació Sprint 3
* Afegir tasca: Definir procés actualització entre develop, stage i producció, tinguent en compte la base de dades.

#### Objectiu de l'sprint
avançar en les històries que han quedat de l'sprint2

#### Sel·lecció llista d'històries
s'han mogut les inacabades a l'sprint3 i afegit alguna de nova

#### Com demostrar-ho
-

#### Històries dividides en tasques
s'ha comentat el flux de treball pel CRUD Empreses