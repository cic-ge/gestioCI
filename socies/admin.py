from django.contrib import admin

from .models import AdrecaProjecteAutoocupat, Activitat, Persona, ContribucioCIC, Habilitat, GrupHabilitat, \
    HabilitatsContribucionsPersona, AdrecaFiscal, AdrecaSociaCooperativa, \
    AdrecaProjecteCollectiu

for model in (AdrecaProjecteAutoocupat,
              AdrecaFiscal,
              Activitat,
              Persona,
              HabilitatsContribucionsPersona,
              ContribucioCIC,
              Habilitat,
              GrupHabilitat,
              AdrecaSociaCooperativa,
              AdrecaProjecteCollectiu,):
    admin.site.register(model)
