# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0004_persona_dni_i_regeneracio_personacercable'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='activitat',
            options={'ordering': ['nom']},
        ),
        migrations.AlterModelOptions(
            name='gruphabilitat',
            options={'ordering': ['nom'], 'verbose_name': "Grup d'habilitats", 'verbose_name_plural': "Grups d'habilitats"},
        ),
        migrations.AlterModelOptions(
            name='habilitat',
            options={'ordering': ['nom'], 'verbose_name': 'Habilitat', 'verbose_name_plural': 'Habilitats'},
        ),
        migrations.AddField(
            model_name='adrecaprojecteautoocupat',
            name='created_at',
            field=models.DateTimeField(default='1970-01-01 10:00:00+0200', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='adrecaprojecteautoocupat',
            name='updated_at',
            field=models.DateTimeField(default='1970-01-01 10:00:00+0200', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='personausuaria',
            name='created_at',
            field=models.DateTimeField(default='1970-01-01 10:00:00+0200', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='personausuaria',
            name='updated_at',
            field=models.DateTimeField(default='1970-01-01 10:00:00+0200', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='preferenciapersonausuaria',
            name='created_at',
            field=models.DateTimeField(default='1970-01-01 10:00:00+0200', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='preferenciapersonausuaria',
            name='updated_at',
            field=models.DateTimeField(default='1970-01-01 10:00:00+0200', auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='adrecaprojecteautoocupat',
            name='activitats',
            field=models.ManyToManyField(related_name='+', to='socies.Activitat'),
        ),
        migrations.AlterField(
            model_name='adrecaprojecteautoocupat',
            name='designacio',
            field=models.CharField(help_text='Nom per identificar aquesta adre\xe7a, especialment si n\'hi ha m\xe9s d\'una al projecte; p.ex. es podrien dir "restaurant" i "botiga".', max_length=128, verbose_name='designaci\xf3'),
        ),
        migrations.AlterField(
            model_name='habilitatscontribucionspersona',
            name='contribucions',
            field=models.ManyToManyField(to='socies.ContribucioCIC', blank=True),
        ),
        migrations.AlterField(
            model_name='habilitatscontribucionspersona',
            name='habilitats',
            field=models.ManyToManyField(to='socies.Habilitat', blank=True),
        ),
        migrations.AlterField(
            model_name='persona',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='personacercable',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
    ]
