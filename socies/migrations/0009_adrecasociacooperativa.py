# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0008_auto_20160205_1853'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdrecaSociaCooperativa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pais', models.CharField(max_length=128, verbose_name='pa\xeds', blank=True)),
                ('provincia', models.CharField(max_length=128, verbose_name='prov\xedncia', blank=True)),
                ('comarca', models.CharField(max_length=128, blank=True)),
                ('poblacio', models.CharField(max_length=128, verbose_name='poblaci\xf3')),
                ('codi_postal', models.CharField(max_length=16)),
                ('adreca', models.CharField(max_length=128, verbose_name='adre\xe7a')),
                ('ubicacio_especifica', models.CharField(max_length=256, verbose_name='ubicaci\xf3 espec\xedfica', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
                'verbose_name': "Adre\xe7a d'una socia de la cooperativa",
                'verbose_name_plural': 'Adre\xe7es de les socies de la cooperativa',
            },
        ),
    ]
