# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0001_initial'),
        ('alta_socies', '0004_procesaltaautoocupat_pas_maxim_assolit'),
    ]

    operations = [
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='socies_afins_addicionals',
            field=models.ManyToManyField(related_name='+', null=True, to='socies.Persona', blank=True),
            preserve_default=True,
        ),
    ]
