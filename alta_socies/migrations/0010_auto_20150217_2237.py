# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0009_fotoprojecteautoocupatfiraire_url_miniatura'),
    ]

    operations = [
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='pas',
            field=models.SmallIntegerField(default=0, help_text="punt del proc\xe9s d'alta en el que es troba el projecte", verbose_name="pas al proc\xe9s d'alta"),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='pas_maxim_assolit',
            field=models.SmallIntegerField(default=0, verbose_name="pas m\xe9s avan\xe7at que s'ha assolit"),
            preserve_default=True,
        ),
    ]
