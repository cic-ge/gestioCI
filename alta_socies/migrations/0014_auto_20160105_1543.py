# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0013_projectecollectiu_sociaafi_sociacooperativa'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sociaafi',
            options={'verbose_name': 'Socia Af\xed', 'verbose_name_plural': 'Socies Afins'},
        ),
    ]
