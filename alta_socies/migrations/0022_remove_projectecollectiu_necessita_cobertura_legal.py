# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0021_sociacooperativa_adreca'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectecollectiu',
            name='necessita_cobertura_legal',
        ),
    ]
