# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0017_auto_20160209_1146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='compte_CES_assignat',
            field=models.CharField(blank=True, max_length=16, null=True, validators=[django.core.validators.RegexValidator(regex=re.compile(b'^(COOP\\d{4})?$'), message='el format de compte_ces_assignat ha de ser COOP9999')]),
        ),
    ]
