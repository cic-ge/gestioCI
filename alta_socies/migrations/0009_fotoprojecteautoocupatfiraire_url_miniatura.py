# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0008_fotoprojecteautoocupatfiraire'),
    ]

    operations = [
        migrations.AddField(
            model_name='fotoprojecteautoocupatfiraire',
            name='url_miniatura',
            field=models.CharField(max_length=1024, null=True, blank=True),
            preserve_default=True,
        ),
    ]
