# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0022_remove_projectecollectiu_necessita_cobertura_legal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sociacooperativa',
            name='quota_alta',
            field=models.CharField(default=b'euro', max_length=12, choices=[(b'euro', b'30'), (b'eco', b'30'), (b'h', b'6')]),
        ),
    ]
