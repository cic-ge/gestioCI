# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0018_auto_20160213_1257'),
    ]

    operations = [
        migrations.RenameField(
            model_name='procesaltaautoocupat',
            old_name='compte_CES_assignat',
            new_name='compte_ces_assignat',
        ),
        migrations.RenameField(
            model_name='projectecollectiu',
            old_name='compte_CES_assignat',
            new_name='compte_ces_assignat',
        ),
        migrations.RenameField(
            model_name='sociacooperativa',
            old_name='compte_CES_assignat',
            new_name='compte_ces_assignat',
        ),
    ]
