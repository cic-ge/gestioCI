# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('projectes', '0002_auto_20160204_1038'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projecteautoocupat',
            name='compte_ces_assignat',
            field=models.CharField(unique=True, max_length=16, validators=[django.core.validators.RegexValidator(regex=re.compile(b'^COOP\\d{4}$'), message='el format de compte_ces_assignat ha de ser COOP9999')]),
        ),
    ]
