# Changelog
`GestioCI` adheres to [Semantic Versioning](https://semver.org).


## 1.42.24 - 2016-05-11 - Obrero Organizado
* Implemented: command to accept step-20 processes.
* Dropped: home feature for SociaCooperativa.
* Dropped: user creation for SociaCooperativa.
* Dropped: membre de referencia for ProjecteCollectiu.
* Fixed: landing page now allows enter ProjecteCollectiu registration form.


## 1.41.20 - 2016-04-22 - Narcotraficante Neurótico
* Al resum de facturació del projecte autoocupat, s'ha dividit el llistat "factures rebudes" en tres parts: acceptades, pendents, rebutjades.
* Ara s'avisa al membre de referència del projecte autoocupat de que el balanç actual no és acurat, si havent passat la data límit no ha fet l'autoliquidació trimestral, mentre GE no faci el tancament defi
nitiu.
* Hem afegit la possibilitat de filtrar per número de pas al cercador de processos d'alta de projecte autoocupat.
* L'informe "estadístiques" dels processos d'alta de projecte autoocupat permet ara clicar al recompte de processos d'alta per anar a revisar-los.
* Hem fet una utilitat que genera un informe amb els membres de cada "grup de permissos" per detectar canvis inesperats.
* S'ha arreglat l'error en el càlcul de l'aportació trimestral.
* Hem afegit una marca als projectes autoocupats que permet saber si es va importar [més o menys automàticament] des del "transversal"; té la connotació de que en cas afirmatiu s'espera una revisió a fons, donat que al transversal no hi ha tota la informació (i la que hi és no està ben tabulada) sobre pólisses, cessions d'ús, etc.), per part tant de GE com del membre de referència.
* A la taula d'aportacions trimestrals, s'ha millorat l'explicació de l'algorisme que es fa servir.
* Afegits camps de control per ProjecteAutoocupat: `created_at` i `updated_at` segons la filosofia de django.
* Petites correccions i millores al codi.

## 1.38.17 - 2016-04-10 - Marqués Miserable
* Improved: Now bank statement comments appear in "concepte" field in balance.
* Improved: CSV export feature for bank statements now has "comments" column.
* Fixed: Maths around QuotaTrimestral now supports negative balance.
* Small fixes.
* Small improvements.

## 1.35.15 - 2016-03-30 - Letrado Leninista
* Fixed: Invoice number sequence starts at 2 instead of 1 each year
* Fixed: "Validació de factures rebudes" is not showing any invoice
* Fixed: Exporting the ExtracteBancari CSV results in a weird filename
* Implemented: Ledger shows entries in chronological order
* Implemented: The report "Resum facturació" is now explicitly sorted
* Implemented: The user is no longer allowed to close the quarter
* Implemented: The user gets a reminder (and a countdown) 15 days before the "periode de tancament"
* Implemented: The user gets a warning (and a countdown) during the "periode de tancament"

## 1.30.12 - 2016-03-28 - Kinki Kamikaze
* Fixed: validation bug
* Implemented: update/read operations for ProjecteCollectiu
* Implemented: update/read operations for SociaCooperativa
* Implemented: book of SociaAfi instances
* Implemented: member's filter by ability feature
* Implemented: book of projects
* Implemented: book of members
* Fixed: improper queryset was leading to workflow cycles
* Small improvements
* Small fixes

## 1.23.9 - 2016-03-13 - Jabalí Jodido
* Implemented: AdrecaFiscal fields on Empresa create/update operations

## 1.22.9 - 2016-03-12 - Insurrecionalista Inmolado
* Minor improvements
* Implemented: "Moviments simples" conciliation feature
* Minor fixes
* Implemented: "Autoliquidar" feature
* Fixed: Trimestre could be repeatedly closed
* Implemented: full-featured "ProjecteAutoocupat" list view
* Implemented: Export "ExtracteBancari" feature

## 1.17.7 - 2016-02-12 - Heliogabalo Hipocrita
* Minor improvements
* Minor fixes
* Implemented: giant steps towards an almost complete 'side-by-side' feature

## 1.15.6 - 2016-02-12 - Guru Gamberro
* Improved: constrain IVA to choices field
* Implemented: list of discarded "MoivmentExtracteBancari" instances
* Implemented: admindocs feature
* Minor fixes
* Minor improvements
* Implemented: WIP 'side-by-side' feature

## 1.10.5 - 2016-02-04 - Falangista Fulminado
* Deprecated: `Factura.data_prevista_pagament` as not desired.
* Improved: Set uniqueness of `ProjecteAutoocupat.compte_ces_assignat` field.
* Deprecated: `Empresa.compte_corrent` field as unused
* Added: Import bank statements feature
* Improved: merged `Empresa.nom` field with `Empresa.nom_fiscal` field
* Added: missing STATICFILES_DIRS to production settings

## 1.4.5 - 2016-02-03 - Etarra Empoderado
* Added: docs/codenames.md
* Added: missing button to reach "validació d'empreses" feature.
* Fixed: broken corner case where there was no closed "Trimestre".
* Fixed: broken URLs redirection.
* Removed: garbage in template.

## 1.2.2 - 2016-02-02 - Dictador Destituido
* Fixed: regression in URLs redirects.
* Fixed: wrong template election in a view.

## 1.2.0 - 2016-01-31 - Camarada Concentrado
* Added: "Altres Altes" module.

## 1.1.0 - 2016-01-28 - Barrendero Bolchevique
* Improved: separe dashboard to improve user testing and minor improvements.

## 1.0.0 - 2016-01-26 - Activista Apalancado
* First release.

